const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

//test('Type your tests here', () => {});

for (let number of [2,4,6,8,10,11,13,15,17,19,20,22,24,26,29,31,33,35]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/black', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [0,1,3,5,7,9,12,14,16,18,21,23,25,27,28,30,32,34,36]) {
    test('Number different then black should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/black', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [1,3,5,7,9,12,14,16,18,21,23,25,27,28,30,32,34,36]) {
    test('Red should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/red', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [0,2,4,6,8,10,11,13,15,17,19,20,22,24,26,29,31,33,35]) {
    test('Number different then red should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/red', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [1,4,7,10,13,16,19,22,25,28,31,34]) {
    test('Column 1 should triple chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/column/1', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [2,3,5,6,8,9,11,12,14,15,17,18,20,21,23,24,26,27,29,30,32,33,35,36]) {
    test('Different then Column 1 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/column/1', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [2,5,8,11,14,17,20,23,26,29,32,35]) {
    test('Column 2 should triple chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/column/2', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [0,1,3,4,6,7,9,10,12,13,15,16,18,19,21,22,24,25,27,28,30,31,33,34,36]) {
    test('Different then Column 2 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/column/2', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [3,6,9,12,15,18,21,24,27,30,33,36]) {
    test('Column 3 should triple chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/column/3', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [0,1,2,4,5,7,8,10,11,13,14,16,17,19,20,22,23,25,26,28,29,31,32,34,35]) {
    test('Different then Column 3 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/column/3', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [0,1,2,3]) {
    test('Corner 0-1-2-3 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/0-1-2-3', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 0-1-2-3 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/0-1-2-3', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [1,2,4,5]) {
    test('Corner 1-2-4-5 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/1-2-4-5', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 1-2-4-5 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/1-2-4-5', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [2,3,5,6]) {
    test('Corner 2-3-5-6 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/2-3-5-6', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 2-3-5-6 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/2-3-5-6', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [4,5,7,8]) {
    test('Corner 4-5-7-8 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/4-5-7-8', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,6,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 4-5-7-8 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/4-5-7-8', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [5,6,8,9]) {
    test('Corner 5-6-8-9 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/5-6-8-9', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,7,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 5-6-8-9 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/5-6-8-9', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [7,8,10,11]) {
    test('Corner 7-8-10-11 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/7-8-10-11', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,9,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 7-8-10-11 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/7-8-10-11', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [8,9,11,12]) {
    test('Corner 8-9-11-12 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/8-9-11-12', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,10,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 8-9-11-12 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/8-9-11-12', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [10,11,13,14]) {
    test('Corner 10-11-13-14 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/10-11-13-14', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,12,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 10-11-13-14 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/10-11-13-14', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [11,12,14,15]) {
    test('Corner 11-12-14-15 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/11-12-14-15', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 11-12-14-15 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/11-12-14-15', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [13,14,16,17]) {
    test('Corner 13-14-16-17 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/13-14-16-17', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,15,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 13-14-16-17 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/13-14-16-17', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [14,15,17,18]) {
    test('Corner 14-15-17-18 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/14-15-17-18', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,16,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 14-15-17-18 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/14-15-17-18', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [16,17,19,20]) {
    test('Corner 16-17-19-20 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/16-17-19-20', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,18,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 16-17-19-20 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/16-17-19-20', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [17,18,20,21]) {
    test('Corner 17-18-20-21 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/17-18-20-21', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,19,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 17-18-20-21 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/17-18-20-21', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [19,20,22,23]) {
    test('Corner 19-20-22-23 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/19-20-22-23', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,21,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 19-20-22-23 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/19-20-22-23', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [20,21,23,24]) {
    test('Corner 20-21-23-24 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/20-21-23-24', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,22,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 20-21-23-24 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/20-21-23-24', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [22,23,25,26]) {
    test('Corner 22-23-25-26 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/22-23-25-26', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,24,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 22-23-25-26 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/22-23-25-26', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [23,24,26,27]) {
    test('Corner 23-24-26-27 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/23-24-26-27', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,25,28,29,30,31,32,33,34,35,36]) {
    test('Different then Corner 23-24-26-27 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/23-24-26-27', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [25,26,28,29]) {
    test('Corner 25-26-28-29 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/25-26-28-29', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,27,30,31,32,33,34,35,36]) {
    test('Different then Corner 25-26-28-29 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/25-26-28-29', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [26,27,29,30]) {
    test('Corner 26-27-29-30 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/26-27-29-30', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,28,31,32,33,34,35,36]) {
    test('Different then Corner 26-27-29-30 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/26-27-29-30', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [28,29,31,32]) {
    test('Corner 28-29-31-32 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/28-29-31-32', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,30,33,34,35,36]) {
    test('Different then Corner 28-29-31-32 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/28-29-31-32', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [29,30,32,33]) {
    test('Corner 29-30-32-33 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/29-30-32-33', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,31,34,35,36]) {
    test('Different then Corner 29-30-32-33 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/29-30-32-33', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [31,32,34,35]) {
    test('Corner 31-32-34-35 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/31-32-34-35', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,33,36]) {
    test('Different then Corner 31-32-34-35 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/31-32-34-35', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [32,33,35,36]) {
    test('Corner 32-33-35-36 should win 8 times chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/32-33-35-36', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34]) {
    test('Different then Corner 32-33-35-36 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/corner/32-33-35-36', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [1,2,3,4,5,6,7,8,9,10,11,12]) {
    test('Dozen 1 should win triple chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/dozen/1', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [0,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34]) {
    test('Different then Dozen 1 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/dozen/1', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [13,14,15,16,17,18,19,20,21,22,23,24]) {
    test('Dozen 2 should win triple chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/dozen/2', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Dozen 2 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/dozen/2', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Dozen 3 should win triple chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/dozen/3', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) {
    test('Different then Dozen 3 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/dozen/3', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36]) {
    test('Even should win double chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/even', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [0,1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35]) {
    test('Different then Even should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/even', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35]) {
    test('Odd should win double chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/odd', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36]) {
    test('Different then odd should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/odd', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('High should win double chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/high', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]) {
    test('Different then high should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/high', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]) {
    test('Low should win double chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/low', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [0,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then low should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/low', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [1,2,3,4,5,6]) {
    test('Line 1-2-3-4-5-6 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/1-2-3-4-5-6', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 1-2-3-4-5-6 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/1-2-3-4-5-6', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [4,5,6,7,8,9]) {
    test('Line 4-5-6-7-8-9 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/4-5-6-7-8-9', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 4-5-6-7-8-9 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/4-5-6-7-8-9', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [7,8,9,10,11,12]) {
    test('Line 7-8-9-10-11-12 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/7-8-9-10-11-12', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 7-8-9-10-11-12 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/7-8-9-10-11-12', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [10,11,12,13,14,15]) {
    test('Line 10-11-12-13-14-15 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/10-11-12-13-14-15', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 10-11-12-13-14-15 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/10-11-12-13-14-15', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [13,14,15,16,17,18]) {
    test('Line 13-14-15-16-17-18 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/13-14-15-16-17-18', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 13-14-15-16-17-18 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/13-14-15-16-17-18', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [16,17,18,19,20,21]) {
    test('Line 16-17-18-19-20-21 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/16-17-18-19-20-21', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 16-17-18-19-20-21 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/16-17-18-19-20-21', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [19,20,21,22,23,24]) {
    test('Line 19-20-21-22-23-24 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/4-5-6-7-8-9', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 19-20-21-22-23-24 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/19-20-21-22-23-24', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [22,23,24,25,26,27]) {
    test('Line 22-23-24-25-26-27 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/22-23-24-25-26-27', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 22-23-24-25-26-27 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/22-23-24-25-26-27', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [25,26,27,28,29,30]) {
    test('Line 25-26-27-28-29-30 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/25-26-27-28-29-30', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,31,32,33,34,35,36]) {
    test('Different then Line 25-26-27-28-29-30 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/25-26-27-28-29-30', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [28,29,30,31,32,33]) {
    test('Line 28-29-30-31-32-33 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/28-29-30-31-32-33', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,34,35,36]) {
    test('Different then Line 28-29-30-31-32-33 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/28-29-30-31-32-33', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

for (let number of [31,32,33,34,35,36]) {
    test('Line 31-32-33-34-35-36 should win times 5 chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/31-32-33-34-35-36', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
    });
}

for (let number of [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Different then Line 31-32-33-34-35-36 should lose all chips if number ' + number + ' is spinned', () => {
        return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post('/bets/line/31-32-33-34-35-36', hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(0))
    });
}

test.each([
   ['0-1-2', 0],
   ['0-1-2', 1],
   ['0-1-2', 2],
   ['0-2-3', 0],
   ['0-2-3', 2],
   ['0-2-3', 3],
   ['1-2-3', 1],
   ['1-2-3', 2],
   ['1-2-3', 3],
   ['4-5-6', 4],
   ['4-5-6', 5],
   ['4-5-6', 6],
   ['7-8-9', 7],
   ['7-8-9', 8],
   ['7-8-9', 9],
   ['10-11-12', 10],
   ['10-11-12', 11],
   ['10-11-12', 12],
   ['13-14-15', 13],
   ['13-14-15', 14],
   ['13-14-15', 15],
   ['16-17-18', 16],
   ['16-17-18', 17],
   ['16-17-18', 18],
   ['19-20-21', 19],
   ['19-20-21', 20],
   ['19-20-21', 21],
   ['22-23-24', 22],
   ['22-23-24', 23],
   ['22-23-24', 24],
   ['25-26-27', 25],
   ['25-26-27', 26],
   ['25-26-27', 27],
   ['28-29-30', 28],
   ['28-29-30', 29],
   ['28-29-30', 30],
   ['31-32-33', 31],
   ['31-32-33', 32],
   ['31-32-33', 33],
   ['34-35-36', 34],
   ['34-35-36', 35],
   ['34-35-36', 36]
])(
  'Bet on street %s should should win times 11 if number %d is spinned',
  (bet, number) =>
    post('/players')
      .then((response) => {
        hashname = response.hashname;
        return post(`/bets/street/${bet}`, hashname, { chips: 100 }); // a bet
      })
      .then((response) => post('/spin/' + number, hashname)) // spin the wheel
      .then((response) => get('/chips', hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(1200))
);

test.each([
    ['0-1', 0],
    ['0-1', 1],
    ['1-2', 1],
    ['1-2', 2],
    ['1-4', 1],
    ['1-4', 4],
    ['2-5', 2],
    ['2-5', 5],
    ['3-6', 3],
    ['3-6', 6],
    ['2-3', 2],
    ['2-3', 3],
])(
    'Bet on split %s should win times 18 if number %d is spinned',
    (bet, number) =>
        post('/players')
            .then((response) => {
                hashname = response.hashname;
                return post(`/bets/split/${bet}`, hashname, { chips: 100 }); // a bet
            })
            .then((response) => post('/spin/' + number, hashname)) // spin the wheel
            .then((response) => get('/chips', hashname)) // checking the number of chips
            .then((response) => expect(response.chips).toEqual(1800))
);

test.each([
    ['0', 0],
    ['1', 1],
    ['2', 2],
    ['3', 3],
    ['4', 4],
    ['5', 5],
    ['6', 6],
    ['7', 7],
    ['8', 8],
    ['9', 9],
    ['10', 10],
    ['11', 11],
    ['12', 12],
    ['13', 13],
    ['14', 14],
    ['15', 15],
    ['16', 16],
    ['17', 17],
    ['18', 18],
    ['19', 19],
    ['20', 20],
    ['21', 21],
    ['22', 22],
    ['23', 23],
    ['24', 24],
    ['25', 25],
    ['26', 26],
    ['27', 27],
    ['28', 28],
    ['29', 29],
    ['30', 30],
    ['31', 31],
    ['32', 32],
    ['33', 33],
    ['34', 34],
    ['35', 35],
    ['36', 36],
])(
    'Bet on straight %s should win times 35 if number %d is spinned',
    (bet, number) =>
        post('/players')
            .then((response) => {
                hashname = response.hashname;
                return post(`/bets/straight/${bet}`, hashname, { chips: 100 }); // a bet
            })
            .then((response) => post('/spin/' + number, hashname)) // spin the wheel
            .then((response) => get('/chips', hashname)) // checking the number of chips
            .then((response) => expect(response.chips).toEqual(3600))
);

// 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36
